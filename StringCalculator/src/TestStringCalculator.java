import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 */

/**
 * JUnit tests created to test the StringCalculator
 * @author Ankita Rastogi
 * @version 1.0
 *
 */
public class TestStringCalculator {

	@Test
	/**
	 * Tests to add comma separated numbers
	 * Example: 1,2,3
	 */
	public void addingCommaSeparatedNumbers() {
        StringCalculator strCal = new StringCalculator(); // MyClass is tested

        // assert statements
        assertEquals(0, strCal.addCommaSeparated(""));
        assertEquals(6, strCal.addCommaSeparated("1,2,3"));
        assertEquals(130, strCal.addCommaSeparated(" 10 ,  90  ,  30 "));
        assertEquals(0, strCal.addCommaSeparated(" 000 ,  0  ,  0 "));
        assertEquals(30, strCal.addCommaSeparated(" -1 ,  10  ,  20 "));
        assertEquals(21, strCal.addCommaSeparated(" 1 ,  1010  ,  20 "));
        assertEquals(20, strCal.addCommaSeparated(" -1 ,  1002  ,  20 "));
        
        assertEquals(6, strCal.addCommaSeparated("1 \n,2,3"));
        assertEquals(130, strCal.addCommaSeparated(" 10\n ,  90  ,  30 "));
        assertEquals(0, strCal.addCommaSeparated(" 000 ,  0  ,  0 "));
        assertEquals(30, strCal.addCommaSeparated(" -1\n ,  10\n  ,  20\n "));
        assertEquals(21, strCal.addCommaSeparated(" 1 ,  1010  ,  20 "));
        assertEquals(20, strCal.addCommaSeparated(" -1 ,  1002  ,  20 "));
    }

	@Test
	/**
	 * Tests to add comma separated numbers but also could have new line
	 * Example "1, 2\n, 3
	 */
	public void addingCommaSeparatedNumbersWithNewLine() {
        StringCalculator strCal = new StringCalculator(); // MyClass is tested

        // assert statements
        assertEquals(0, strCal.addCommaSeparated(""));
        assertEquals(7, strCal.addCommaSeparated("1 \n,3,3"));
        assertEquals(50, strCal.addCommaSeparated(" 10\n ,  10  ,  30 "));
        assertEquals(0, strCal.addCommaSeparated(" 00 ,  0  ,  0 "));
        assertEquals(35, strCal.addCommaSeparated(" -1\n ,  15\n  ,  20\n "));
        assertEquals(50, strCal.addCommaSeparated(" 30 ,  1010  ,  20 "));
        assertEquals(20, strCal.addCommaSeparated(" -1 ,  1002  ,  20 "));
    }
	
	@Test
	/**
	 * Tests to add numbers delimited by custom delimiters and also could have new line
	 * Example "//;\n1; 2; 3
	 */
	public void addingNumbersSeparatedByCustomDelimiter() {
        StringCalculator strCal = new StringCalculator(); // MyClass is tested

        // assert statements
        assertEquals(0, strCal.addCustomDelimiter(""));
        assertEquals(11, strCal.addCustomDelimiter("//;\n1;7;3"));
        assertEquals(70, strCal.addCustomDelimiter("//;\n 10  ;  57  ;  3"));
        assertEquals(0, strCal.addCustomDelimiter(" //;\n 0  ;  00  ;  0 "));
        assertEquals(121, strCal.addCustomDelimiter(" //@\n 10  @  21  @  90 "));
        assertEquals(121, strCal.addCustomDelimiter(" //@\n 10  @  81  @  30 "));
        assertEquals(111, strCal.addCustomDelimiter("// *\n 1000 *  21 * 90 "));
    }
}
