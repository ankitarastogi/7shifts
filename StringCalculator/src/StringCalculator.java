import java.util.Scanner;

/**
 * Class to create a string calculator in which string is passed which 
 * contains comma separated numbers or custom delimiter separated numbers.
 * @author Ankita Rastogi
 *
 */
public class StringCalculator {

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {

		StringCalculator c = new StringCalculator();
		System.out.println("Trying to enter empty string (Returns 0): " +  c.addCommaSeparated(""));
		System.out.println("Adding 1, 7, and 3 : " +  c.addCommaSeparated("1,2,3"));
		System.out.println("Adding 1, 7, and 3 : " + c.addCustomDelimiter("//;\n1;7;3"));
		System.out.println("Adding 10, 71 and 90 delimited by custom delimiter @ : " + c.addCustomDelimiter("//@\n 10  @  71  @  90"));
		System.out.println("Adding 1000, 31 and 90 delimited by custom delimiter *  : " + c.addCustomDelimiter("//*\n 1000 *  31 * 90 "));
		System.out.println("Adding -1000, 1 and 90 delimited by custom delimiter *  : " + c.addCustomDelimiter("//*\n -1000 *  1 * 90 "));
	
	}


	/**
	 *  ****************SOLUTION TO QUESTION 3 AND 4 AND BONUS(1) **********************
	 * This method add the numbers passed in form of string formatted
	 * "//$\n1$2$3"
	 * @param orgString The passed in string with delimiters and control
	 * @return Integer : addition of numbers
	 */
	public int addCustomDelimiter(String orgString) {
		//Initializing variables
		int numToAdd = 0;
		int addition = 0;
		String subStr = "";
		
		//Check to see if the originial(numbers to add) string is empty. If empty, addition returns 0
		//Example String = "//*\n 2 * 3* 4"
		if (orgString != "") {
			char delimiter = getDelimiter(orgString); //Calling the helper method to return the delimiter in string. Returns with *
			
			//Get the index of delimiter. Returns 2
			int indexOfDelimiter = orgString.trim().indexOf(delimiter);

			//Trimming and get the subString to get the string of numbers, because passing 3
			//returns the remaining string starting from 3
			// Returns 20 * 30* 40
			String strToCut = orgString.trim().substring(indexOfDelimiter + 1);
		
			while (strToCut.trim().length() > 0) {
				//In the number string with delimiters find the location of delimiter
				//Returns 2
				indexOfDelimiter = strToCut.trim().indexOf(delimiter);
				
				//If the delimiter exists
				if (indexOfDelimiter > 0) {
					//get the substring (number) from the beginning of the string to the delimiter. (Returns 20, then 30)
					subStr = strToCut.trim().substring(0, indexOfDelimiter);
					numToAdd = Integer.parseInt(subStr.trim()); //Trim and get the number to add
					strToCut = strToCut.trim().substring(indexOfDelimiter + 1); //Substring to get the remaining string
				} else {
					numToAdd = Integer.parseInt(strToCut.trim()); //If the delimiter is not found add the last number of the string

					strToCut = strToCut.trim().substring(strToCut.trim().length()); //Updating the length of the string
				}
				//Check for not adding negative numbers and ignoring numbers greater than 1000.
				if (numToAdd > 0 && numToAdd < 1000) {

					addition += numToAdd;

				}
				
				//throws an exception if negative number passed in string
				//Returns the adding ignoring the negative numbers
				if (numToAdd < 0) {
					try {
						throw new Exception("Negatives not allowed. Remove the negaive sign from " + numToAdd);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		return addition; //Addition of numbers
	}

	/**
	 * This is a helper method to find the delimiter from the string
	 * Method could be reused anywhere  if the delimiter is entered before a new line
	 *[//;\n]
	 * @param orgString Orginal string with delimiter
	 * @return Char - delimiter
	 */
	private char getDelimiter(String orgString) {
		String delimiterStr = "";
		char delimiter = ' ';

		if (orgString != "") {
			Scanner scanner = new Scanner(orgString); //Opened a scanner and passed the original string

			//Check if the string has a new line character
			if (scanner.hasNextLine()) {
				delimiterStr = scanner.nextLine();
				
				//Getting the delimiter 
				delimiter = delimiterStr.charAt(delimiterStr.length() - 1);
			}
			scanner.close();
		}
		return delimiter;
	}
	
	/**
	 ****************SOLUTION TO QUESTION 1 AND 2 **********************
	 * This function adds the numbers which are comma separated and could have a new line character
	 * 
	 * @param orgString Original String of numbers passed
	 * @return Integer Addition of numbers
	 */
	public int addCommaSeparated(String orgString) 
	{
		int numToAdd = 0;
		int addition = 0;
		String subStr = "";

		if (orgString != "") {
			String strToCut = orgString;
			while (strToCut.trim().length() > 0) {
				//Getting the index of comma
				int indexOfComma = strToCut.trim().indexOf(',');
				if (indexOfComma > 0) {
					//Substring to get the integers
					subStr = strToCut.trim().substring(0, indexOfComma);
					//parse the number to integer
					numToAdd = Integer.parseInt(subStr.trim());
					//Substring to get the remainign string after the first string
					strToCut = strToCut.trim().substring(indexOfComma + 1);
				} else {
					//If no comma is left get the last integer in the string
					numToAdd = Integer.parseInt(strToCut.trim());
					strToCut = strToCut.trim().substring(strToCut.trim().length());
				}
				//Add only positive numbers and numbers less than 1000
				if (numToAdd > 0 && numToAdd < 1000) {
					addition += numToAdd;
				}
			}

		}
		return addition;
	}
}